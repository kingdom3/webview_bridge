package com.example.myapplication

import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import java.time.LocalDateTime


class MainActivity : AppCompatActivity(), AndroidBridge.BridgeListener {

    private val bridge = AndroidBridge()
    var mTextView:TextView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.webview)
        WebView.setWebContentsDebuggingEnabled(true)
        val myWebView: WebView = findViewById(R.id.webview)

        myWebView.loadUrl("file:///android_asset/webViewTest.html")
        myWebView.apply {
            webViewClient = WebViewClient()
            settings.javaScriptEnabled = true
        }
        myWebView.addJavascriptInterface(bridge, "AndroidBridge")
        bridge.setListener(this)

        val button =findViewById<Button>(R.id.transfer);
        mTextView=findViewById<TextView>(R.id.textView)

        button.setOnClickListener(object: View.OnClickListener{
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onClick(v: View?) {
                val time: LocalDateTime = LocalDateTime.now()
                myWebView.loadUrl("javascript:androidToWeb('Android > Web 전송<br>"+time+"<br>Hello JavaScript')");
            }
        })
    }

    override fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        mTextView?.text=msg
    }
}
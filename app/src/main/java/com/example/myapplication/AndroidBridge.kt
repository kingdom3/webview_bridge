package com.example.myapplication

import android.webkit.JavascriptInterface
import androidx.appcompat.app.AppCompatActivity


class AndroidBridge  : AppCompatActivity() {

    private var callback: BridgeListener? = null

    fun setListener(listener: BridgeListener) {
        callback = listener
    }

    @JavascriptInterface
    fun makeToast(msg: String) {
        callback?.showToast(msg)
    }

    interface BridgeListener {
        fun showToast(msg: String)
    }
}